﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPData.PersonData
{
    public class PersonalData
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public string Email { get; set; }

        public PersonalData()
        {
        }

        public PersonalData(string Name, string Surname, string Street, string Town)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Street = Street;
            this.Town = Town;
        }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",Name,Surname,Street,Town);
        }







    }
}
