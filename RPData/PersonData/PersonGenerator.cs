﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPData.PersonData
{
    public class PersonGenerator
    {
        private Random _random;
        public PersonGenerator()
        {
           _random = new Random();
        }
        public PersonalData Generate()
        {
           
            PersonalData pers = new PersonalData();

            pers.Surname = SurnamesTemplates.Surnames[_random.Next(0, SurnamesTemplates.Count())];
            pers.Street = StreetsTemplates.Streets[_random.Next(0,StreetsTemplates.Count())];
            pers.Name = NamesTemplates.Names[_random.Next(0, NamesTemplates.Count())];
            pers.Town = TownsTemplates.Towns[_random.Next(0, TownsTemplates.Count())];

            return pers;
        }
    }
}
