﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPData.PESEL
{
    public class PeselGenerator
    {
        private Random _random { get; set; }

        public PeselGenerator()
        {
            _random = new Random();

        }

        //TODO generowanie calkowicie randomowego peselu
        public PESEL Generate()
        {
            PESEL pesel = new PESEL();
            GenerateYear(pesel);
            GenerateMonth(pesel);
            GenerateDay(pesel);
            GenerateSerialNumber(pesel);
            GenerateSex(pesel);
            GenerateControlNumber(pesel);

            return pesel;
        }

        public PESEL Generate(bool sex)
        {
            PESEL pesel = new PESEL();

            GenerateYear(pesel);
            GenerateMonth(pesel);
            GenerateDay(pesel);
            GenerateSerialNumber(pesel);
            GenerateControlNumber(pesel);

            if (sex == true)
            {
                var sexTemp = _random.Next(0, 9);

                if (sexTemp % 2 != 0)
                    sexTemp++;

                pesel.Sex = sexTemp.ToString();
            }


            return pesel;
        }

        public PESEL Generate(string year = null, string month = null, string day = null, string serialNumber=null, string sex=null, string controlNumber=null)
        {
            PESEL pesel = new PESEL();
            if(year == null)
            {
                GenerateYear(pesel);
            }
            else
            {
                pesel.Year = year;
            }

            if(controlNumber == null)
            {
                GenerateControlNumber(pesel);
            }
            else
            {
                pesel.ControlNumber = controlNumber;
            }
            if(sex == null)
            {
                GenerateSex(pesel);
            }
            else
            {
                pesel.Sex = sex;
            }
            if(month == null)
            {
                GenerateMonth(pesel);
            }
            else
            {
                pesel.Month = month;
            }
            if(day ==null)
            {
                GenerateDay(pesel);
            }
            else
            {
                pesel.Day = day;
            }
            if(serialNumber == null)
            {
                GenerateSerialNumber(pesel);
            }
            else
            {
                pesel.SerialNumber = serialNumber;
            }
               

            return new PESEL();
        }




        public bool Validate(PESEL pesel)
        {
            return false;
        }


        private void GenerateControlNumber(PESEL pesel)
        {
            pesel.ControlNumber = _random.Next(0, 9).ToString();
        }

        private void GenerateSex(PESEL pesel)
        {
            pesel.Sex = _random.Next(0, 9).ToString();
        }

        private void GenerateSerialNumber(PESEL pesel)
        {
            int SerialNumber = _random.Next(0, 999);

            if (SerialNumber < 10)
                pesel.SerialNumber = string.Format("00{0}", SerialNumber);
            else if (SerialNumber < 100)
                pesel.SerialNumber = string.Format("0{0}", SerialNumber);
            else
                pesel.SerialNumber = SerialNumber.ToString();
        }

        private void GenerateDay(PESEL pesel)
        {
            int Day = _random.Next(1, 30);

            if (Day < 10)
                pesel.Day = string.Format("0{0}", Day);
            else
                pesel.Day = Day.ToString();
        }

        private void GenerateMonth(PESEL pesel)
        {
            int Month = _random.Next(1, 12);

            if (Month < 10)
                pesel.Month = string.Format("0{0}", Month);
            else
                pesel.Month = Month.ToString();
        }

        private void GenerateYear(PESEL pesel)
        {
            pesel.Year = _random.Next(30, 99).ToString();
        }


        //TODO generowanie pesela w zaleznosci od tego czy jest kobieta czy mezczyzna
        
	}
}

