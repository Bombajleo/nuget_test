﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPData.PESEL
{
	public class PESEL
	{
		public string Year { get; set; }
		public string Month { get; set; }
		public string Day { get; set; }
		public string SerialNumber { get; set; }
		public string Sex { get; set; }
		public string ControlNumber { get; set; }

		public PESEL()
		{

		}


        public PESEL(string year, string month, string day, string serialNumber, string sex, string controlNumber)
        {

            Year = year;
            Month = month;
            Day = day;
            SerialNumber = serialNumber;
            Sex = sex;
            ControlNumber = controlNumber;
        }

        
        public override string ToString()
		{           
            return string.Format("{0}{1}{2}{3}{4}{5}", Year, Month, Day, SerialNumber, Sex, ControlNumber);
		}		
	}
}
