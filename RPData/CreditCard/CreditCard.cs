﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPData.CreditCard
{
    public class CreditCard
    {
        public string CreditCardNumber { get; set; }
        public string CVV { get; set; }
        public string PIN { get; set; }
        public DateTime ValideDate { get; set; }
    }
}
