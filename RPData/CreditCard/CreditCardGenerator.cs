﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RPData.CreditCard
{
    public class CreditCardGenerator
    {
        private Random _rand { get; set; }


        /// <summary>
        /// Converting into string
        /// </summary>
        /// <returns></returns>
        public string CreateNumber()
        {
            int[] list = CreateNumber(16);
            string card = string.Empty;
            foreach (var l in list)
                card += l;

            return card;
        }

        /// <summary>
        /// Method that create a valid credit card using Luhn Algoritm.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public int[] CreateNumber(int length)
        {

            Random random = new Random();
            int[] digits = new int[length];
            for (int i = 0; i < length - 1; i++)
            {
                digits[i] = random.Next(10);
            }
            int sum = 0;
            bool alt = true;
            for (int i = length - 2; i >= 0; i--)
            {
                int temp = digits[i];
                if (alt)
                {
                    temp *= 2;
                    if (temp > 9)
                    {
                        temp -= 9; //equivalent to adding numbers to each other, for example 1 + 6 = 7, 16-9 = 7
                    }
                }
                sum += temp;
                alt = !alt;
            }
            int modulo = sum % 10;
            if (modulo > 0)
            {
                modulo = 10 - modulo;
            }
            digits[length - 1] = modulo;

            return digits;

        }

        /// <summary>
        /// Method wchich ckeck is card is valid (should be valid allways).
        /// </summary>
        /// <param name="digits"></param>
        /// <returns></returns>
        public bool CheckNumber(int[] digits)
        {
            int sum = 0;
            bool alt = false;
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int temp = digits[i];
                if (alt)
                {
                    temp *= 2;
                    if (temp > 9)
                    {
                        temp -= 9; //equivalent to adding numbers to each other, for example 1 + 6 = 7, 16-9 = 7
                    }
                }
                sum += temp;
                alt = !alt;
            }
            return sum % 10 == 0;
        }
        private DateTime RandomDay()
        {
            var now = DateTime.Now;
            var Begin = now.AddDays(- 2 * 365);
            Begin.AddDays(_rand.Next(7 * 365));
            return Begin;
        }

        public CreditCard GenerateCard()
        {
            CreditCard creditCard = new CreditCard();
            creditCard.CVV = _rand.Next(0, 999).ToString();
            creditCard.PIN = _rand.Next(0, 9999).ToString();
            creditCard.CreditCardNumber = CreateNumber();
            creditCard.ValideDate = RandomDay();
            return creditCard;
        }
    }

}