﻿using RPData;
using RPData.PESEL;
using RPData.PersonData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPData.CreditCard;

namespace Nugget
{
    class Program
    {
        static void Main(string[] args)
        {
            PESEL pesel = new PESEL("99", "12", "05", "256", "1", "6");

            Console.WriteLine(pesel.ToString());

            PeselGenerator peselGenerator = new PeselGenerator();
            

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(peselGenerator.Generate().ToString());

            }

            peselGenerator.Generate(controlNumber: "555", month: "03");


            PersonGenerator personGenerator = new PersonGenerator();
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(personGenerator.Generate().ToString());
            }


            CreditCardGenerator creditCardGenerator = new CreditCardGenerator();
            creditCardGenerator.GenerateCard();
            

            Console.Read();
        }
    }
}
